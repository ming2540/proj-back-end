#include <Python.h>
#include <bytesobject.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <vector>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <cmath>

using namespace std;

typedef struct {
    int score = 0;
    vector<int> perfect;
    vector<string> pronounce;
    vector<int> interRhyme;
    vector<int> intraRhyme;
    vector<int> tone;
    vector<int> alphabetRhyme;
    vector<int> interChapterRhyme;
}Result;

const char vowels[] = {
    'a','e','i','o','u'
};
#define VOWELS_COUNT 5

//prototype
string translateDataToCommand(string datas, string type);
vector<vector<string>> thpronunTextToSectionSyllable(const char *cmd);
//end of prototype



// begin poem analyze

vector<string> getPronounce(vector<int> position, vector<string> buffer){
    vector<string> result;
    // cout << "hello";
    // for(auto p: position){
    //     cout << p << '\n';
    // }
    for(size_t i=0; i<buffer.size(); i++){
        string cmd = translateDataToCommand(buffer.at(i), "t"); 
        vector<vector<string>> pronouces = thpronunTextToSectionSyllable(cmd.c_str());
        if(pronouces.size() > position.at(i)){
            for(auto syllabel : pronouces.at(position.at(i))){
                // cout << syllabel;
                result.push_back(syllabel);
            }
        } else {
            result.push_back("");
        }
    }
    return result;
}


bool checkRhyme(string sylA, string sylB){
    if(sylA == "" || sylB == ""){
        return false;
    }
    if(sylA == sylB){
        return false;
    }
    size_t sizeA = sylA.size()-1;
    size_t sizeB = sylB.size()-1;

    for(int i=0; i<VOWELS_COUNT; i++){
        if(sizeA > sylA.find(vowels[i]))
            sizeA = sylA.find(vowels[i]);
        if(sizeB > sylB.find(vowels[i]))
            sizeB = sylB.find(vowels[i]);
    }

    string vowelA = sylA.substr(sizeA);
    vowelA = vowelA.erase(vowelA.size()-1);
    string vowelB = sylB.substr(sizeB);
    vowelB = vowelB.erase(vowelB.size()-1);

    cout << vowelA << " vs " << vowelB << "\n";

    return (vowelA.compare(vowelB) == 0)? true : false;
}

bool checkDeadSyllable(string sylA){
    if(sylA == ""){
        return false;
    }

    int sizeA = sylA.size()-1;
    char finalConsonant = sylA.at(sizeA-1);
    if(finalConsonant == 't' || finalConsonant == 'k' || finalConsonant == 'p' || finalConsonant == 'h'){
        return true;
    }

    return false;
}

bool checkAlphabet(string sylA, string sylB){
    if(sylA == "" || sylB == ""){
        return false;
    }
    
    size_t sizeA = sylA.size()-1;
    size_t sizeB = sylB.size()-1;

    for(int i=0; i<VOWELS_COUNT; i++){//size of vowels in thpronuns
        if(sizeA > sylA.find(vowels[i]))
            sizeA = sylA.find(vowels[i]);
        if(sizeB > sylB.find(vowels[i]))
            sizeB = sylB.find(vowels[i]);
    }
    string alphabetA = sylA.substr(0, sizeA);
    string alphabetB = sylB.substr(0, sizeB);

    return (alphabetA.compare(alphabetB) == 0)? true : false;
}

int checkInterSectionRhyme(vector<string> sectionA, vector<string> sectionB, int type){
    int result = 0;

    if(sectionA.size() == 0 || sectionB.size() == 0) {
        result = 0;
    } else {
        switch (type)
        {
        case 1:
        case 3: if(sectionA.size() > 0 && sectionB.size() > 2){
                    if(checkRhyme(sectionA.at(sectionA.size()-1), sectionB.at(2)) ||
                    checkRhyme(sectionA.at(sectionA.size()-1), sectionB.at(2)))
                    result = 1;
                }
                break;
        case 2:
        case 4:
            if(checkRhyme(sectionA.at(sectionA.size()-1), sectionB.at(sectionB.size()-1)))
               result = 1;
            break;
        //four poem case
        case 5: if(sectionB.size() > 1){
                    if(checkRhyme(sectionA.at(sectionA.size()-1), sectionB.at(1)))
                        result = 1;
                }
                break;            
        default:
            break;
        }
    }

    return result;
}

int checkIntraSectionRhyme(vector<string> section, int type){
    int result = 0;

    if(section.size() == 0){
        return 0;
    }
    switch(type){
        case 1: if(section.size() > 4){
                    if(checkRhyme(section.at(2), section.at(3))){
                        result+=1;
                    }
                }
                if(section.size() > 7){
                    if(checkRhyme(section.at(4), section.at(5)) || checkRhyme(section.at(4), section.at(6))){
                        result+=1;
                    }
                }
                break;
        case 2: for(int i=0; i<section.size()-1; i++){

                    int j=i+1;
                    while(j<section.size()  && checkRhyme(section.at(i), section.at(j))){
                        result++;
                        j++;
                    }
                    i=j-1;
                }
                break;
    }
    
    return result;
}

int checkAlphabetRhyme(vector<string> section){
    int result=0;
    
    if(section.size() == 0){
        return 0;
    }

    for(int i=0; i<section.size()-1; i++){

        int j=i+1;
        while(j<section.size()  && checkAlphabet(section.at(i), section.at(j))){
            result++;
            j++;
        }
        i=j-1;
    }
    return result;
}

int checkTone(vector<string> section, int type){
    if(section.size() == 0){
        return 0;
    }

    string lastSyllabel = section.at(section.size()-1);
    char tone = lastSyllabel.at(lastSyllabel.size()-1);
    int result = 0;
    // 0 means bad, 1 means ok, 2 means good
    
    switch(type){
        case 1: if(tone == '0')
                    result = 1;
                else 
                    result = 2;
                break;
        case 2: if(tone == '0' || tone == '3')
                    result = 0;
                else if(tone == '4')
                    result = 2;
                else 
                    result = 1;
                break;
        case 3: // same as case 4
        case 4: if(tone == '0' || tone == '3')
                    result = 2;
                else 
                    result = 0;
                break;
        default: break;
    }

    return result;
}

int findToneCharactor(string word, int tone){
    size_t found;

    switch(tone){
        case 1: found = word.find('่');
                break;
        case 2: found = word.find('้');
                break;
    }
    if(found != string::npos){
        return 1;
    }
    return 0;
}

int checkFourPoemCharacterTone(vector<string> section, int type){
    if(section.size() == 0){
        return 0;
    }

    int result = 0;
    size_t sectionSize = section.size();
    int tone = 0;

    string selectedSyllabel;
    switch(type){
        case 1: selectedSyllabel = section.at(section.size()-2);
                break;
        case 2:
        case 5:
        case 9: selectedSyllabel = section.at(section.size()-1);
                break;
        case 3: 
        case 7:
        case 8: 
        case 11:selectedSyllabel = section.at(1);
                break;
        case 4: 
        case 10: selectedSyllabel = section.at(0);
                break;
        case 6: selectedSyllabel = section.at(2);
                break;
    }
    switch(type){
        case 1:
        case 3:
        case 4:
        case 6:
        case 7:
        case 8:
        case 10: tone = 1;
                 break;
        case 2:
        case 5:
        case 9:
        case 11: tone = 2;
                 break;


    }

    result = findToneCharactor(selectedSyllabel, tone);

    return result;
}

int checkFourPoemSoundTone(vector<string> section, int type){
    if(section.size() == 0){
        return 0;
    }

    string selectedSyllabel;
    switch(type){
        case 1: selectedSyllabel = section.at(section.size()-2);
                break;
        case 2:
        case 5:
        case 9: selectedSyllabel = section.at(section.size()-1);
                break;
        case 3: 
        case 7:
        case 8: 
        case 11:selectedSyllabel = section.at(1);
                break;
        case 4: 
        case 10: selectedSyllabel = section.at(0);
                break;
        case 6: selectedSyllabel = section.at(2);
                break;
    }

    char tone = selectedSyllabel.at(selectedSyllabel.size()-1);
    int result = 0;
    switch(type){
        case 1: if(tone == '1' || checkDeadSyllable(selectedSyllabel)){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 2: if(tone == '2'){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 3: if(tone == '1' || checkDeadSyllable(selectedSyllabel)){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 4: if(tone == '1' || checkDeadSyllable(selectedSyllabel)){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 5: if(tone == '2'){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 6: if(tone == '1' || checkDeadSyllable(selectedSyllabel)){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 7: if(tone == '1' || checkDeadSyllable(selectedSyllabel)){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 8: if(tone == '1' || checkDeadSyllable(selectedSyllabel)){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 9: if(tone == '2'){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
         case 10: if(tone == '1' || checkDeadSyllable(selectedSyllabel)){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
        case 11: if(tone == '2'){
                        result = 1;
                } else{
                    result = 0;
                }
                break;
    }

    return result;
}

int checkFourPoemTone(vector<string> section, vector<string> sectionRaw, int type){
    if(checkFourPoemCharacterTone(sectionRaw, type) || checkFourPoemSoundTone(section, type)){
        return 1;
    }
    return 0;
}

int isPerfectEightPoem(vector<string> sadab,
vector<string> rub,
vector<string> rong,
vector<string> song) {
    if(sadab.size() == 8 && rub.size() == 8 && rong.size() == 8 && song.size() == 8){
        return 1;
    }
    return 0;
}

int eightPoemCalculateRhyme(vector<string> sadab,
    vector<string> rub,
    vector<string> rong,
    vector<string> song,
    vector<string> lastSong){

    int score = 0;

    int sadabSize = int(sadab.size());
    int rubSize = int(rub.size());
    int rongSize = int(rong.size());
    int songSize =  int(song.size());
    int lastSongSize =  int(lastSong.size());


    if(sadabSize > 0 && rubSize > 0){
        score += checkInterSectionRhyme(sadab, rub, 1) * 15;
    }
    if(rubSize > 0 && rongSize > 0){
        score += checkInterSectionRhyme(rub, rong, 2) * 15;
    }
    if(rongSize > 0 && songSize > 0){
        score += checkInterSectionRhyme(rong, song, 3) * 15;
    }
    if(sadabSize > 0){
        score += checkIntraSectionRhyme(sadab, 1) * 3 +
                    checkAlphabetRhyme(sadab) +
                    checkTone(sadab, 1) * 3;
        if(sadabSize != 8){
            score -= abs(sadabSize-8)*3;
        }
    }
    if(rubSize > 0){
        score += checkIntraSectionRhyme(rub, 1)* 3 +
                    checkAlphabetRhyme(rub) +
                    checkTone(rub, 2) * 3;
        if(rubSize != 8){
            score -= abs(rubSize-8)*3;
        }
    }
    if(rubSize > 0 && lastSongSize > 0){
        score += checkInterSectionRhyme(rub, lastSong, 4);
    }
    if(rongSize > 0){
        score += checkIntraSectionRhyme(rong, 1) * 3 +
                    checkAlphabetRhyme(rong) +
                    checkTone(rong, 3) * 3;
        if(rongSize != 8){
            score -= abs(rongSize-8)*3;
        }
    }
    if(songSize > 0){
        score += checkIntraSectionRhyme(song, 1) * 3 +
                    checkAlphabetRhyme(song) +
                    checkTone(song, 4) * 3;
        if(songSize != 8){
            score -= abs(songSize-8)*3;
        }
    }
    // cout << score << '\n';
    // cout << "-----------------------------------" << "\n";
    return score;
}

int fourPoemCalculateRhyme(vector<string> firstSec,
    vector<string> secondSec,
    vector<string> thirdSec,
    vector<string> forthSec,
    vector<string> fifthSec,
    vector<string> sixthSec,
    vector<string> seventhSec,
    vector<string> eigthSec,
    vector<string> lastEight,
    vector<vector<string>> datas_sub){

    int score = 0;

    int firstSize = int(firstSec.size());
    int secondSize = int(secondSec.size());
    int thirdSize = int(thirdSec.size());
    int forthSize =  int(forthSec.size());
    int fifthSize = int(fifthSec.size());
    int sixthSize = int(sixthSec.size());
    int seventhSize = int(seventhSec.size());
    int eigthSize =  int(eigthSec.size());

    // cout << firstSize << " " << secondSize << " " << thirdSize << " " << forthSize << " ";
    // cout << fifthSize << " " << sixthSize << " " << seventhSize << " " << eigthSize << " " << '\n';
    // cout << "------------------------------" << '\n';

    if(secondSize > 1 && thirdSize > 4 && fifthSize > 4){
        //นอก
        score += checkInterSectionRhyme(thirdSec, secondSec, 5)*15;
                 checkInterSectionRhyme(fifthSec, secondSec, 5)*15;
    }
    if(forthSize > 1 && seventhSize > 4){
        //นอก
        score += checkInterSectionRhyme(seventhSec, forthSec, 5)*15;
    }
    if(firstSize > 0){
        if(firstSize > 3){
            score += checkFourPoemTone(firstSec, datas_sub.at(0), 1)*3;
        }
        if(firstSize > 4){
            score += checkFourPoemTone(firstSec, datas_sub.at(0), 2)*3;
        }
        if(firstSize == 5){
            score += 3;
        }
        score += checkIntraSectionRhyme(firstSec, 2);
        score += checkAlphabetRhyme(firstSec);
    }
    if(secondSize > 0){
        if(secondSize==2 || secondSize==4){
            score += 3;
        }
        score += checkIntraSectionRhyme(secondSec, 2);
        score += checkAlphabetRhyme(secondSec);
    }
    if(thirdSize > 0){
        if(thirdSize > 1){
            score += checkFourPoemTone(firstSec, datas_sub.at(2), 1)*3;
        }
        if(thirdSize == 5){
            score += 3;
        }
        score += checkIntraSectionRhyme(thirdSec, 2);
        score += checkAlphabetRhyme(thirdSec);
    }
    if(forthSize > 0){
        score += checkFourPoemTone(forthSec, datas_sub.at(3), 4)*3;
        if(firstSize > 1){
            score += checkFourPoemTone(forthSec, datas_sub.at(3), 5)*3;
        }
        if(forthSize == 2){
            score += 3;
        }
        score += checkIntraSectionRhyme(forthSec, 2);
        score += checkAlphabetRhyme(forthSec);
    }
    if(fifthSize > 0){
        if(fifthSize > 2){
            score += checkFourPoemTone(fifthSec, datas_sub.at(4), 6)*3;
        }
        if(fifthSize == 5){
            score += 3;
        }
        score += checkIntraSectionRhyme(fifthSec, 2);
        score += checkAlphabetRhyme(fifthSec);
    }
    if(sixthSize > 0){
        if(sixthSize > 1){
            score += checkFourPoemTone(sixthSec, datas_sub.at(5), 7)*3;
        }
        if(sixthSize==2 || sixthSize==4){
            score += 3;
        }
        score += checkIntraSectionRhyme(sixthSec, 2);
        score += checkAlphabetRhyme(sixthSec);
    }
    if(seventhSize > 0){
        if(seventhSize > 1){
            score += checkFourPoemTone(seventhSec, datas_sub.at(6), 8)*3;
        }
        if(seventhSize > 4){
            score += checkFourPoemTone(seventhSec, datas_sub.at(6), 9)*3;
        }
        if(seventhSize == 5){
            score += 3;
        }
        score += checkIntraSectionRhyme(seventhSec, 2);
        score += checkAlphabetRhyme(seventhSec);
    }
    if(eigthSize > 0){
        if(eigthSize > 0){
            score += checkFourPoemTone(eigthSec, datas_sub.at(7), 9)*3;
        }
        if(eigthSize > 1){
            score += checkFourPoemTone(eigthSec, datas_sub.at(7), 10)*3;
        }
        if(eigthSize == 4){
            score += 3;
        }
        score += checkIntraSectionRhyme(eigthSec, 2);
        score += checkAlphabetRhyme(eigthSec);
    }
    // cout << score << '\n';
    return score;
}

Result fourPoemAnalyze(vector<vector<vector<string>>> poem, vector<string> buffer, vector<vector<string>> buffer_sub){
    Result result;
    int score = 0;
    int checkEmpty[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    int tempBestPos[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    vector<int> bestPosition;
    vector<vector<string>> firstSec;
    vector<vector<string>> secondSec;
    vector<vector<string>> thirdSec;
    vector<vector<string>> forthSec;
    vector<vector<string>> fifthSec;
    vector<vector<string>> sixthSec;
    vector<vector<string>> seventhSec;
    vector<vector<string>> eigthSec;
    vector<string> lastEight;

    // cout << "poemsize = " << buffer_sub.size() << '\n';

    for(int poemIndex=0; poemIndex<poem.size(); poemIndex+=8){
        score = 0;
        for (int i=0; i<8; i++){
            checkEmpty[i] = 0;
            if(poemIndex+i >= poem.size()){
                break;
            }
            // cout << i;
            switch(i){
                case 0: firstSec.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 1: secondSec.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 2: thirdSec.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 3: forthSec.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 4: fifthSec.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 5: sixthSec.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 6: seventhSec.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 7: eigthSec.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
            }
        }
        int firstSize = int(firstSec.size());
        int secondSize = int(secondSec.size());
        int thirdSize = int(thirdSec.size());
        int forthSize =  int(forthSec.size());
        int fifthSize = int(fifthSec.size());
        int sixthSize = int(sixthSec.size());
        int seventhSize = int(seventhSec.size());
        int eigthSize =  int(eigthSec.size());
        if(firstSize == 0){
            firstSize+=2;
            checkEmpty[0]=1;
        }
        if(secondSize == 0){
            secondSize+=2;
            checkEmpty[1]=1;
        }
        if(thirdSize == 0){
            thirdSize+=2;
            checkEmpty[2]=1;
        }
        if(forthSize == 0){
            forthSize+=2;
            checkEmpty[3]=1;
        }
        if(fifthSize == 0){
            fifthSize+=2;
            checkEmpty[4]=1;
        }
        if(sixthSize == 0){
            sixthSize+=2;
            checkEmpty[5]=1;
        }
        if(seventhSize == 0){
            seventhSize+=2;
            checkEmpty[6]=1;
        }
        if(eigthSize == 0){
            eigthSize+=2;
            checkEmpty[7]=1;
        }
        vector<string> tempFirst;
        vector<string> tempSecond;
        vector<string> tempThird;
        vector<string> tempForth;
        vector<string> tempFifth;
        vector<string> tempSixth;
        vector<string> tempSeventh;
        vector<string> tempEigth;
        
        for(int i = 0; i < firstSize-1; i++){
            for(int j = 0; j < secondSize-1; j++){
                for(int k = 0; k < thirdSize-1; k++){
                    for(int l = 0; l < forthSize-1; l++){
                        for(int m = 0; m < fifthSize-1; m++){
                            for(int n = 0; n < sixthSize-1; n++){
                                for(int o = 0; o < seventhSize-1; o++){
                                    for(int p = 0; p < eigthSize-1; p++){
                                        // cout << i << " " << j << " " << k << " " << l << " " << m << " " << n << " " << o << " " << p << '\n';
                                        // cout << firstSize << " " << secondSize << " " << thirdSize << " " << forthSize << " ";
                                        // cout << fifthSize << " " << sixthSize << " " << seventhSize << " " << eigthSize << " " << '\n';
                                        if(!checkEmpty[0] == 1){
                                            tempFirst.assign(firstSec.at(i).begin(), firstSec.at(i).end());
                                        }
                                        if(!checkEmpty[1] == 1){
                                            tempSecond.assign(secondSec.at(j).begin(), secondSec.at(j).end());
                                        }
                                        if(!checkEmpty[2] == 1){
                                            tempThird.assign(thirdSec.at(k).begin(), thirdSec.at(k).end());
                                        }
                                        if(!checkEmpty[3] == 1){
                                            tempForth.assign(forthSec.at(l).begin(), forthSec.at(l).end());
                                        }
                                        if(!checkEmpty[4] == 1){
                                            tempFifth.assign(fifthSec.at(m).begin(), fifthSec.at(m).end());
                                        }
                                        if(!checkEmpty[5] == 1){
                                            tempSixth.assign(sixthSec.at(n).begin(), sixthSec.at(n).end());
                                        }
                                        if(!checkEmpty[6] == 1){
                                            tempSeventh.assign(seventhSec.at(o).begin(), seventhSec.at(o).end());
                                        }
                                        if(!checkEmpty[7] == 1){
                                            tempEigth.assign(eigthSec.at(p).begin(), eigthSec.at(p).end());
                                        }
                                        if(score < fourPoemCalculateRhyme(tempFirst, tempSecond, tempThird, tempForth,
                                                                          tempFifth, tempSixth, tempSeventh, tempEigth, lastEight, buffer_sub)){
                                            score = fourPoemCalculateRhyme(tempFirst, tempSecond, tempThird, tempForth,
                                                                          tempFifth, tempSixth, tempSeventh, tempEigth, lastEight, buffer_sub);                                       // cout << score;
                                            tempBestPos[0] = i;
                                            tempBestPos[1] = j;
                                            tempBestPos[2] = k;
                                            tempBestPos[3] = l;
                                            tempBestPos[4] = m;
                                            tempBestPos[5] = n;
                                            tempBestPos[6] = o;
                                            tempBestPos[7] = p;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        bestPosition.push_back(tempBestPos[0]);
        bestPosition.push_back(tempBestPos[1]);
        bestPosition.push_back(tempBestPos[2]);
        bestPosition.push_back(tempBestPos[3]);
        bestPosition.push_back(tempBestPos[4]);
        bestPosition.push_back(tempBestPos[5]);
        bestPosition.push_back(tempBestPos[6]);
        bestPosition.push_back(tempBestPos[7]);

        result.score = score;
        if(checkEmpty[1] == 0 && checkEmpty[2] == 0 && checkEmpty[4] == 0){
            result.interRhyme.push_back(checkInterSectionRhyme(thirdSec.at(bestPosition.at(2)), secondSec.at(bestPosition.at(1)), 5));
            result.interRhyme.push_back(checkInterSectionRhyme(fifthSec.at(bestPosition.at(4)), secondSec.at(bestPosition.at(1)), 5));
        } else {
            result.interRhyme.push_back(0);
            result.interRhyme.push_back(0);
        }
        if(checkEmpty[4] == 0 && checkEmpty[6] == 0){
            result.interRhyme.push_back(checkInterSectionRhyme(seventhSec.at(bestPosition.at(6)), forthSec.at(bestPosition.at(3)), 5));
        }
        if(checkEmpty[0] == 0){
            result.tone.push_back(checkFourPoemTone(firstSec.at(bestPosition.at(0)), buffer_sub.at(0), 1));
            result.tone.push_back(checkFourPoemTone(firstSec.at(bestPosition.at(0)), buffer_sub.at(0), 2));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(firstSec.at(bestPosition.at(0))));
            result.intraRhyme.push_back(checkIntraSectionRhyme(firstSec.at(bestPosition.at(0)), 2));
        } else {
            result.tone.push_back(0);
            result.tone.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.intraRhyme.push_back(0);
        }
        if(checkEmpty[1] == 0){
            result.alphabetRhyme.push_back(checkAlphabetRhyme(secondSec.at(bestPosition.at(1))));
            result.intraRhyme.push_back(checkIntraSectionRhyme(secondSec.at(bestPosition.at(1)), 2));
        }else {
            result.alphabetRhyme.push_back(0);
            result.intraRhyme.push_back(0);
        }
        if(checkEmpty[2] == 0){
            result.tone.push_back(checkFourPoemTone(thirdSec.at(bestPosition.at(2)),  buffer_sub.at(2), 3));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(thirdSec.at(bestPosition.at(2))));
            result.intraRhyme.push_back(checkIntraSectionRhyme(thirdSec.at(bestPosition.at(2)), 2));
        } else {
            result.tone.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.intraRhyme.push_back(0);
        }
        if(checkEmpty[3] == 0){
            result.tone.push_back(checkFourPoemTone(forthSec.at(bestPosition.at(3)),  buffer_sub.at(3), 4));
            result.tone.push_back(checkFourPoemTone(forthSec.at(bestPosition.at(3)),  buffer_sub.at(3), 5));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(forthSec.at(bestPosition.at(3))));
            result.intraRhyme.push_back(checkIntraSectionRhyme(forthSec.at(bestPosition.at(3)), 2));
        } else {
            result.tone.push_back(0);
            result.tone.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.intraRhyme.push_back(0);
        }
        if(checkEmpty[4] == 0){
            result.tone.push_back(checkFourPoemTone(fifthSec.at(bestPosition.at(4)),  buffer_sub.at(4), 6));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(fifthSec.at(bestPosition.at(4))));
            result.intraRhyme.push_back(checkIntraSectionRhyme(fifthSec.at(bestPosition.at(4)), 2));
        } else {
            result.tone.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.intraRhyme.push_back(0);
        }
        if(checkEmpty[5] == 0){
            result.tone.push_back(checkFourPoemTone(sixthSec.at(bestPosition.at(5)),  buffer_sub.at(5), 7));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(sixthSec.at(bestPosition.at(5))));
            result.intraRhyme.push_back(checkIntraSectionRhyme(sixthSec.at(bestPosition.at(5)), 2));
        } else {
            result.tone.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.intraRhyme.push_back(0);
        }
        if(checkEmpty[6] == 0){
            result.tone.push_back(checkFourPoemTone(seventhSec.at(bestPosition.at(6)),  buffer_sub.at(6), 8));
            result.tone.push_back(checkFourPoemTone(seventhSec.at(bestPosition.at(6)),  buffer_sub.at(6), 9));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(seventhSec.at(bestPosition.at(6))));
            result.intraRhyme.push_back(checkIntraSectionRhyme(seventhSec.at(bestPosition.at(6)), 2));
        } else {
            result.tone.push_back(0);
            result.tone.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.intraRhyme.push_back(0);
        }
        if(checkEmpty[7] == 0){
            result.tone.push_back(checkFourPoemTone(eigthSec.at(bestPosition.at(7)),  buffer_sub.at(7), 10));
            result.tone.push_back(checkFourPoemTone(eigthSec.at(bestPosition.at(7)),  buffer_sub.at(7), 11));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(eigthSec.at(bestPosition.at(7))));
            result.intraRhyme.push_back(checkIntraSectionRhyme(eigthSec.at(bestPosition.at(7)), 2));
        } else {
            result.tone.push_back(0);
            result.tone.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.intraRhyme.push_back(0);
        }
    }

    result.pronounce = getPronounce(bestPosition, buffer);
    // for(auto p : result.pronounce){
    //     cout << p << '\n';
    // }
    // cout << result.score;
    return result;
}

Result eightPoemAnalyze(vector<vector<vector<string>>> poem, vector<string> buffer){
    Result result;
    int score = 0;
    int checkEmpty[4] = {0, 0, 0, 0};
    int tempBestPos[4] = {0, 0, 0, 0};
    vector<int> bestPosition;
    vector<vector<string>> sadab;
    vector<vector<string>> rub;
    vector<vector<string>> rong;
    vector<vector<string>> song;
    vector<string> lastSong;

    // cout << "poemsize = " << poem.size() << '\n';

    for(int poemIndex=0; poemIndex<poem.size(); poemIndex+=4){
        score = 0;
        for (int i=0; i<4; i++){
            checkEmpty[i] = 0;
            if(poemIndex+i >= poem.size()){
                break;
            }
            // cout << i;
            switch(i){
                case 0: sadab.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 1: rub.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 2: rong.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
                case 3: song.assign(poem.at(poemIndex+i).begin(), poem.at(poemIndex+i).end());
                        break;
            }
            
        }

        int sadabSize = int(sadab.size());
        int rubSize = int(rub.size());
        int rongSize = int(rong.size());
        int songSize =  int(song.size());
        if(sadabSize == 0){
            sadabSize+=2;
            checkEmpty[0]=1;
        }
        if(rubSize == 0){
            rubSize+=2;
            checkEmpty[1]=1;
        }
        if(rongSize == 0){
            rongSize+=2;
            checkEmpty[2]=1;
        }
        if(songSize == 0){
            songSize+=2;
            checkEmpty[3]=1;
        }
        vector<string> tempSadab;
        vector<string> tempRub;
        vector<string> tempRong;
        vector<string> tempSong;
        // for(int i=0;i<4;i++){
        //     cout << checkEmpty[i] << " ";
        // }
        // cout << '\n';
        for(int i = 0; i < sadabSize-1; i++){
            for(int j = 0; j < rubSize-1; j++){
                for(int k = 0; k < rongSize-1; k++){
                    for(int l = 0; l < songSize-1; l++){
                        if(!checkEmpty[0] == 1){
                            tempSadab.assign(sadab.at(i).begin(), sadab.at(i).end());
                        }
                        if(!checkEmpty[1] == 1){
                            tempRub.assign(rub.at(j).begin(), rub.at(j).end());
                        }
                        if(!checkEmpty[2] == 1){
                            tempRong.assign(rong.at(k).begin(), rong.at(k).end());
                        }
                        if(!checkEmpty[3] == 1){
                            tempSong.assign(song.at(l).begin(), song.at(l).end());
                        }
                        // cout << i << " " << j << " " << k << " " << l << '\n';
                        // cout << tempSadab.size() << " " << tempRub.size() << " " << tempRong.size() << " " << tempSong.size() << '\n';
                        if(score < eightPoemCalculateRhyme(tempSadab, tempRub, tempRong, tempSong, lastSong)){
                            score = eightPoemCalculateRhyme(tempSadab, tempRub, tempRong, tempSong, lastSong);
                            // cout << score << " ";
                            tempBestPos[0] = i;
                            tempBestPos[1] = j;
                            tempBestPos[2] = k;
                            tempBestPos[3] = l;
                        }
                    }
                }
            }
        }

        bestPosition.push_back(tempBestPos[0]);
        bestPosition.push_back(tempBestPos[1]);
        bestPosition.push_back(tempBestPos[2]);
        bestPosition.push_back(tempBestPos[3]);
        result.score += score;
        if(checkEmpty[0] == 0 && checkEmpty[1] == 0){
            result.interRhyme.push_back(checkInterSectionRhyme(sadab.at(bestPosition.at(poemIndex)), rub.at(bestPosition.at(poemIndex+1)), 1));  
        } else {
            result.interRhyme.push_back(0);
        }
        if(checkEmpty[1] == 0 && checkEmpty[2] == 0){
            result.interRhyme.push_back(checkInterSectionRhyme(rub.at(bestPosition.at(poemIndex+1)), rong.at(bestPosition.at(poemIndex+2)), 2));
        } else {
            result.interRhyme.push_back(0);
        }
        if(checkEmpty[2] == 0 && checkEmpty[3] == 0){
            result.interRhyme.push_back(checkInterSectionRhyme(rong.at(bestPosition.at(poemIndex+2)), song.at(bestPosition.at(poemIndex+3)), 3));
        } else {
            result.interRhyme.push_back(0);
        }
        if(checkEmpty[0] == 0){
            result.intraRhyme.push_back(checkIntraSectionRhyme(sadab.at(bestPosition.at(poemIndex)), 1));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(sadab.at(bestPosition.at(poemIndex))));
            result.tone.push_back(checkTone(sadab.at(bestPosition.at(poemIndex)), 1));
        } else {
            result.intraRhyme.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.tone.push_back(0);
        }
        if(checkEmpty[1] == 0){            
            result.intraRhyme.push_back(checkIntraSectionRhyme(rub.at(bestPosition.at(poemIndex+1)), 1));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(rub.at(bestPosition.at(poemIndex+1))));
            result.tone.push_back(checkTone(rub.at(bestPosition.at(poemIndex+1)), 2));
            if(lastSong.size() > 0){
                result.interChapterRhyme.push_back(checkInterSectionRhyme(rub.at(bestPosition.at(poemIndex+1)), lastSong, 4));
            } else if(poemIndex > 4){
                result.interChapterRhyme.push_back(0);
            }
        } else {
            result.intraRhyme.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.tone.push_back(0);
        }
        if(checkEmpty[2] == 0){
            result.intraRhyme.push_back(checkIntraSectionRhyme(rong.at(bestPosition.at(poemIndex+2)), 1));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(rong.at(bestPosition.at(poemIndex+2))));
            result.tone.push_back(checkTone(rong.at(bestPosition.at(poemIndex+2)), 3));
        } else {
            result.intraRhyme.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.tone.push_back(0);
        }
        if(checkEmpty[3] == 0){
            result.intraRhyme.push_back(checkIntraSectionRhyme(song.at(bestPosition.at(poemIndex+3)), 1));
            result.alphabetRhyme.push_back(checkAlphabetRhyme(song.at(bestPosition.at(poemIndex+3))));
            result.tone.push_back(checkTone(song.at(bestPosition.at(poemIndex+3)), 4));
            // cout << "songSize: " << songSize << '\n';
            // cout << "best Position Song: " << bestPosition.at(poemIndex+3) << '\n';
            // cout << "song at best size: " << song.at(bestPosition.at(poemIndex+3)).begin() << '\n';
            lastSong.assign(song.at(bestPosition.at(poemIndex+3)).begin(), song.at(bestPosition.at(poemIndex+3)).end());
        } else {
            result.intraRhyme.push_back(0);
            result.alphabetRhyme.push_back(0);
            result.tone.push_back(0);
        }
        if(checkEmpty[0] == 0 && checkEmpty[1] == 0 && checkEmpty[2] == 0 && checkEmpty[3] == 0) {
            result.perfect.push_back(isPerfectEightPoem(sadab.at(bestPosition.at(poemIndex)),
                                                        rub.at(bestPosition.at(poemIndex+1)),
                                                        rong.at(bestPosition.at(poemIndex+2)),
                                                        song.at(bestPosition.at(poemIndex+3))));
        } else {
            result.perfect.push_back(0);
        }
    }
    // cout << "before pronounce";
    result.score /= ceil(poem.size()/4.0);
    if(result.score > 100)
        result.score = 100;
    result.pronounce = getPronounce(bestPosition, buffer);
    // cout << "score" << result.score << '\n';
    return result;
}

// end of poem anaylze

// begin of thpronun translator
string translateDataToCommand(string datas, string type) {
    string command = "thpronun -" + type + " " + datas;
    if(datas.length()==0){
        command = "";
    }
    return command;
}

vector<string> sectionToSyllable(string wuk){
    vector<string> payang;
    istringstream iss(wuk);
    string temp = "";

    while(iss >> temp){
        payang.push_back(temp);
    }
    return payang;
}

vector<vector<string>> thpronunTextToSectionSyllable(const char *cmd) {
    int count=0;
    array<char, 128> buffer;
    vector<string> sections;
    vector<vector<string>> result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen() failed!");
    }
    // cout << buffer.data();
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        if(count>0)
            sections.push_back( buffer.data() );
        count++;
    }
    for(auto syllable : sections){
        // cout << syllable << '\n';
        result.push_back(sectionToSyllable(syllable));
    }
    return result;
}

Result callAnalyze(vector<string> buffer, int type, vector<vector<string>> buffer_sub){
    vector<vector<vector<string>>> poem;
    Result result;

    for(size_t i=0; i<buffer.size(); i++){
        // cout << buffer.at(i) << '\n' ;
        string cmd = translateDataToCommand(buffer.at(i), "p"); 
        // cout << cmd << '\n';
        poem.push_back(thpronunTextToSectionSyllable(cmd.c_str()));
    }

    // cout << type;
    switch(type){
        case 1: result = eightPoemAnalyze(poem, buffer);
                break;
        case 2: result = fourPoemAnalyze(poem, buffer, buffer_sub);
                break;
        default: break;
    }

    return result;
}

// end of poem translate

static PyObject *SpamError;

PyObject *vectorToList_String(vector<string> dataList){
    PyObject *pyList = PyList_New(dataList.size());
    if(!pyList)
        throw logic_error("unable to allocate memory for list");
    for(size_t i=0; i!=dataList.size(); i++){
        PyObject *data = PyUnicode_FromString(dataList.at(i).c_str());
        if(!data)
            throw logic_error("unable to allocate memmory for string");
        PyList_SetItem(pyList, i, data);
    }

    return pyList;
}

PyObject *vectorToList_Int(vector<int> dataList){
    PyObject *pyList = PyList_New(dataList.size());
    if(!pyList)
        throw logic_error("unable to allocate memory for list");
    for(size_t i=0; i!=dataList.size(); i++){
        PyObject *data = PyLong_FromLong(dataList.at(i));
        if(!data)
            throw logic_error("unable to allocate memmory for string");
        PyList_SetItem(pyList, i, data);
    }

    return pyList;
}

static PyObject *
poemAnalyze(PyObject *self, PyObject *args){
    Result result;
    // PyObject *result;
    vector<vector<string>> datas_sub;
    vector<string> datas;
    string buffer;
    PyObject *obj;
    int type;

    if(!PyArg_ParseTuple(args, "Oi", &obj, &type))
        return NULL;
    
    if(!PyList_Check(obj))
        throw logic_error("not a list");
    for(Py_ssize_t i=0; i<PyList_Size(obj); i++){
        PyObject *sub_obj = PyList_GetItem(obj, i);
        if(!PyList_Check(sub_obj))
            throw logic_error("not a list in list");
        // cout << PyList_Size(sub_obj);
        vector<string> data;
        string s = "";
        for(Py_ssize_t j=0; j<PyList_Size(sub_obj); j++){
            PyObject *iter = PyList_GetItem(sub_obj, j);
            PyObject *str = PyUnicode_AsEncodedString(iter, "utf-8", "~E~");
            data.push_back( PyBytes_AS_STRING(str));
            s += PyBytes_AS_STRING(str);
        }
        datas.push_back(s); 
        datas_sub.push_back(data);
    }

    // return Py_BuildValue("s", "hello");
    result = callAnalyze(datas, type, datas_sub);

    return Py_BuildValue("{s : i, s : O, s : O, s : O, s : O, s : O, s : O, s:O}", "score", result.score
                                                                    ,"perfect", vectorToList_Int(result.perfect)
                                                                    ,"pronounce", vectorToList_String(result.pronounce)
                                                                    ,"inter_rhyme", vectorToList_Int(result.interRhyme)
                                                                    ,"intra_rhyme", vectorToList_Int(result.intraRhyme)
                                                                    ,"tone", vectorToList_Int(result.tone)
                                                                    ,"alphabet_rhyme", vectorToList_Int(result.alphabetRhyme)
                                                                    ,"inter_chapter_rhyme", vectorToList_Int(result.interChapterRhyme)
                                                                    );
}

static PyMethodDef poem_analyze_method[]={
    {"poemAnalyze", poemAnalyze, METH_VARARGS,
     "Execute a shell command."},
    {NULL, NULL , 0, NULL}//sentinel
};

static struct PyModuleDef poem_analyze_module{
    PyModuleDef_HEAD_INIT,
    "poem_analyze",//name of module
    NULL, // doc
    -1,
    poem_analyze_method
};


PyMODINIT_FUNC
PyInit_poem_analyze(void){
    return PyModule_Create(&poem_analyze_module);
}

// Result poemAnalyzeSample(){
//     Result result;
//     vector<string> datas;
//     vector<vector<string>> datas_sub;
    

//     // datas.push_back("ตะกอน");
//     // datas.push_back("สงบ");
//     // datas.push_back("แล้ว");
//     // datas_sub.push_back(datas);
//     // datas.clear();
//     // // // datas.push_back("");
//     // datas.push_back("กลับ");
//     // datas.push_back("มา");
//     // datas_sub.push_back(datas);
//     // datas.clear();

//     // datas.push_back("ขุ่น");
//     // datas.push_back("คลัก");
//     // datas.push_back("อีก");
//     // datas.push_back("ครา");
//     // datas_sub.push_back(datas);
//     // datas.clear();

//     // datas.push_back("สุด");
//     // datas.push_back("ใซ้");
//     // datas_sub.push_back(datas);
//     // datas.clear();

//     // datas.push_back("คน");
//     // datas.push_back("ตัด");
//     // datas.push_back("ปัญหา");
//     // datas_sub.push_back(datas);
//     // datas.clear();

//     // // // datas.push_back("");
//     // datas.push_back("เหตุ");
//     // datas.push_back("แห่ง");
//     // datas.push_back("เคือง");
//     // datas.push_back("แล");
//     // datas_sub.push_back(datas);
//     // datas.clear();

//     // datas.push_back("ข่ม");
//     // datas.push_back("จิต");
//     // datas.push_back("สงบ");
//     // datas.push_back("ไว้");
//     // datas_sub.push_back(datas);
//     // datas.clear();

//     // datas.push_back("อย่า");
//     // datas.push_back("ได้");
//     // datas.push_back("ฟื้น");
//     // datas.push_back("ความ");
//     // datas_sub.push_back(datas);
//     // datas.clear();

//     // datas.push_back("ตะกอนสงบแล้ว");
//     // datas.push_back("กลับมา");
//     // datas.push_back("ขุ่นคลักอีกครา");
//     // datas.push_back("สุดใซ้");
//     // datas.push_back("คนตัดปัญหา");
//     // datas.push_back("เหตุแห่งเคืองแล");
//     // datas.push_back("ข่มจิตสงบไว้");
//     // datas.push_back("อย่าได้ฟื้นความ");


//     datas.push_back("เสียง");
//     datas.push_back("ลือ");
//     datas.push_back("เสียง");
//     datas.push_back("เล่า");
//     datas.push_back("อ้าง");
//     datas_sub.push_back(datas);
//     datas.clear();
//     // // datas.push_back("");
//     datas.push_back("อัน");
//     datas.push_back("ใด");
//     datas.push_back("พี่");
//     datas.push_back("เอย");
//     datas_sub.push_back(datas);
//     datas.clear();

//     datas.push_back("เสียง");
//     datas.push_back("ย่อม");
//     datas.push_back("ยอยก");
//     datas.push_back("ใคร");
//     datas_sub.push_back(datas);
//     datas.clear();

//     datas.push_back("ทั่ว");
//     datas.push_back("หล้า");
//     datas_sub.push_back(datas);
//     datas.clear();

//     datas.push_back("สอง");
//     datas.push_back("เขือ");
//     datas.push_back("พี่");
//     datas.push_back("หลับ");
//     datas.push_back("ใหล");
//     datas_sub.push_back(datas);
//     datas.clear();

//     // // datas.push_back("");
//     datas.push_back("ลืม");
//     datas.push_back("ตื่น");
//     datas.push_back("รือ");
//     datas.push_back("พี่");
//     datas_sub.push_back(datas);
//     datas.clear();

//     datas.push_back("สอง");
//     datas.push_back("พี่");
//     datas.push_back("คิด");
//     datas.push_back("เอง");
//     datas.push_back("อ้า");
//     datas_sub.push_back(datas);
//     datas.clear();

//     datas.push_back("อย่า");
//     datas.push_back("ได้");
//     datas.push_back("ถาม");
//     datas.push_back("เผือ");
//     datas_sub.push_back(datas);
//     datas.clear();

//     datas.push_back("เสียงลือเสียงเล่าอ้าง");
//     datas.push_back("อันใดพี่เอย");
//     datas.push_back("เสียงย่อมยอยศใคร");
//     datas.push_back("ทั่วหล้า");
//     datas.push_back("สองเขือพี่หลับใหล");
//     datas.push_back("ลืมตื่นรือพี่");
//     datas.push_back("สองพี่คิดเองอ้า");
//     datas.push_back("อย่าได้ถามเผือ");

//     // datas.push_back("ตะกอนสงบแล้ว");
//     // datas.push_back("กลับมา");
//     // datas.push_back("ขุ่นคลักอีกครา");
//     // datas.push_back("สุดใซ้");
//     // datas.push_back("คนตัดปัญหา");
//     // datas.push_back("เหตุแห่งเคืองแล");
//     // datas.push_back("ข่มจิตสงบไว้");
//     // datas.push_back("อย่าได้ฟื้นความ");

//     // datas.push_back("");
//     // datas.push_back("โอ้จำใจไกลนุชสุดสวาท");
//     // datas.push_back("จึงนิราศเรื่องรักเป็นอักษร");
//     // datas.push_back("ให้เห็นอกตกยากเมื่อจากจร");
//     // datas.push_back("ไปดงดอนแดนป่าพนาวัน");
//     // datas.push_back("กับศิษย์น้องสองนายล้วนชายหนุ่ม");
//     // datas.push_back("น้อยกับพุ่มเพื่อนไร้ในไพรสัณฑ์");
//     // datas.push_back("กับนายแสงแจ้งทางกลางอารัญ");
//     // datas.push_back("จะพากันแรมทางไปต่างเมือง");

//     // datas.push_back("ถึงเถาวัลย์พันเกี่ยวที่เลี้ยวลด");
//     // datas.push_back("ก็ไม่คดเหมือนหนึ่งในน้ำใจคน");


//     for(auto data : datas){
//         cout << data << '\n';
//     }

//     result = callAnalyze(datas, 1, datas_sub);
//     return result;
// }

// int main(){
//     Result result = poemAnalyzeSample();
//     cout << "score = " << result.score << '\n';
//     cout << "pronounce:" << '\n';
//     for(auto a : result.pronounce){
//         cout << a << '\n';
//     }
//     cout << '\n';   
//     cout << "interRhyme = ";
//     for(auto a : result.interRhyme){
//         cout << a << " ";
//     }
//     cout << '\n';
//     cout << "intraRhyme = ";
//     for(auto a : result.intraRhyme){
//         cout << a << " ";
//     }
//     cout << '\n';
//     cout << "alphabetRhyme = ";
//     for(auto a : result.alphabetRhyme){
//         cout << a << " ";
//     }
//     cout << '\n';
//     cout << "interChapterRhyme = ";
//     for(auto a : result.interChapterRhyme){
//         cout << a << " ";
//     }
//     cout << '\n';
//     cout << "tone = ";
//     for(auto a : result.tone){
//         cout << a << " ";
//     }
//     cout << '\n';

//     // cout << checkRhyme("khrueen3", "tueen1");
    
//     // cout << "Result: score = " << result.score << '\n';
// }