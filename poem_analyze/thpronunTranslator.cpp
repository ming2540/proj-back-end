#include <vector>
#include <string>
#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef struct {
    int score = 0;
    bool perfect = true;
    vector<string> pronounce;
    vector<int> interRhyme;
    vector<int> intraRhyme;
    vector<int> tone;
    vector<int> alphabetRhyme;
    vector<int> interChapterRhyme;
}Result;

const char *translateDataToCommand(string datas, string type) {
    string command = "thpronun -" + type + " " + datas;

    return command.c_str();
}

vector<vector<string>> thpronunTextToSectionSyllable(const char *cmd) {
    int count=0;
    array<char, 128> buffer;
    vector<string> sections;
    vector<vector<string>> result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        // if(count>0)
        sections.push_back( buffer.data() );
        // count++;
    }
    for(auto syllable : sections)
        result.push_back(sectionToSyllable(syllable));
    return result;
}

vector<string> sectionToSyllable(string wuk){
    vector<string> payang;
    istringstream iss(wuk);
    string temp = "";

    while(iss >> temp){
        payang.push_back(temp);
    }
    return payang;
}

Result callAnalyze(vector<string> buffer){
    vector<vector<vector<string>>> poem;

    for(size_t i=0; i<buffer.size(); i++){
        const char *cmd = translateDataToCommand(buffer.at(i), "p"); 
        poem.push_back(thpronunTextToSectionSyllable(cmd));
    }
    cout << 'hello';
}

// Result call_alklorn(vector<string> buffer){
//     vector<vector<vector<string>>> klorn;
//     string command = "thpronun -p ";
    
//     for(int i=0; i < buffer.size(); i++){
//         command.append(buffer.at(i));
//         klorn.push_back(text_to_payangs( command.c_str() ) );
//         command.erase(command.begin()+12,command.end() );
//     }

//     return alklornrithm(klorn,buffer);
// }
