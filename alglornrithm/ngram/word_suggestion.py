import random
import dill as pickle
from nltk.metrics import edit_distance
from collections import defaultdict
from .. import models
# from .word_segmentation import word_segmentation
from flask import g


def word_suggestion(text):
    result = []
    r = random.random()
    accumulator = .0
    ngram = pickle.load(open('alglornrithm/ngram/model.pkl', 'rb'))

    for word in ngram[tuple(text[-2:])].keys():
        accumulator += ngram[tuple(text[-2:])][word]
        # select words that are above the probability threshold
        if accumulator >= r:
            result.append(word)
            break
    return result


def word_correction(contents):
    result = {}
    for content in contents:
        # texts = word_segmentation(content)
        # print('Input = ',contents)
        # print('output from word segment tation = ', texts)
        for text in content:
            if not len(text) > 4:
                continue
            splited_texts = word_split(text)
            print('split', splited_texts)
            for st in splited_texts:
                correct_word = models.Vocab.objects(content__exact=text)
                vocabs = models.Vocab.objects(content__contains=st)
                for vocab in vocabs:
                    # print('found: ', vocab.content)
                    # print('word distance = ', edit_distance(vocab.content, text))
                    if edit_distance(vocab.content, text) <= 2 and edit_distance(vocab.content, text) > 0 and not correct_word:
                        result[text] = vocab.content
    # print('result = ', result)
    return result


def word_split(text):
    result = []
    for i in range(0, len(text)):
        result.append(text[:i] + text[i+2:])
    # print(result)
    return result


# model = pickle.load(open('model.pkl', 'rb'))

# print('get input: ')
# inp = input()
# text = inp.split()

# print('predicting...')
# print(dict(model[text[0], text[1]]))

# # for w1_w2 in model:
# #     print(model[w1_w2].keys())
# #     for word in model[w1_w2]:
# #         print(': ', word)

# # text = ["ยัง", "มี"]
# # sentence_finished = False

# # cnt = 1
# # while not sentence_finished:
# #     # select a random probability threshold  
# #     r = random.random()
# #     accumulator = .0
# #     # print('cnt: ', cnt)
# #     for word in model[tuple(text[-2:])].keys():
# #         accumulator += model[tuple(text[-2:])][word]
# #         # select words that are above the probability threshold
# #         if accumulator >= r:
# #             text.append(word)
# #             break
# #         # print(text)

# #     if text[-2:] == [None, None]:
# #         sentence_finished = True
# #     cnt += 1
# r = random.random()
# accumulator = .0

# for word in model[tuple(text[-2:])].keys():
#     accumulator += model[tuple(text[-2:])][word]
#     # select words that are above the probability threshold
#     if accumulator >= r:
#         text.append(word)
#         break

# print (' '.join([t for t in text if t]))
