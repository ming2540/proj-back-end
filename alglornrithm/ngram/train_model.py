from nltk import ngrams
from collections import defaultdict
import random
import re
import pickle
import dill as pickle
import glob, os


model = defaultdict(lambda: defaultdict(lambda: 0))

def text_cleaner(text):
    newString = text
    newString = newString.replace('|', ' ')
    newString = newString.replace('!', ' ')
    newString = newString.replace('?', ' ')
    newString = newString.replace('"', '')
    newString = newString.replace('...', '')
    newString = re.sub('<NE>', '', newString)
    newString = re.sub('</NE>', '', newString)
    newString = re.sub('<POEM>', '', newString)
    newString = re.sub('</POEM>', '', newString)
    newString = re.sub('<AB>', '', newString)
    newString = re.sub('</AB>', '', newString)

    # words = newString.split()
    return newString.split()


def read_text_from_file():
    sentences = []
    for file in glob.glob("alglornrithm/ngram/novel/*.txt"):
        print('read data from file path: ', file)
        with open(file) as fp:
            line = fp.readline
            while line:
                if not line:
                    break
                line = fp.readline()
                # print(text_cleaner(line))
                sentences.append(text_cleaner(line))
    return sentences


# model = defaultdict(dd_return_dd)
file_count = 0

print('training model...')
print('reading data from file...')

sentences = read_text_from_file()

print('reading done!')

print('processing trigram...')

for sentence in sentences:
    for w1, w2, w3 in ngrams(sentence, 3, pad_left=True, pad_right=True):
        model[(w1, w2)][w3] += 1

print('finished processing trigram!')
print('processing model...')

print('size of model[w1_w2]: ', len(model))

for w1_w2 in model:
    total_count = float(sum(model[w1_w2].values()))
    for w3 in model[w1_w2]:
        model[w1_w2][w3] /= total_count
print('finished processing model!')

# pickle.dump(model, 'save_model.pkl')
print('saving model to path: ', save_path)
pickle.dump(model, open('alglornrithm/ngram/model.pkl', 'wb'))
print('done saving!')

print('finished training model!')

