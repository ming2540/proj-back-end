import alglornrithm


def main():
    app = alglornrithm.create_app()

    app.run(
        debug=True,
        host='0.0.0.0',
        port=8080
    )