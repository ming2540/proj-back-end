import mongoengine as me
from datetime import datetime
from flask_user import UserMixin


class Vocab(me.Document):
    meta = {'collection': 'vocabs'}

    content = me.StringField(unique=True)
    
    
