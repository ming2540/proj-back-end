import mongoengine as me
from datetime import datetime
from flask_user import UserMixin


class PoemComment(me.EmbeddedDocument):
    author = me.ReferenceField('User', dbref=True)
    comment_date = me.DateTimeField(required=True,
                                    default=datetime.now)
    content = me.StringField()
    like = me.IntField()


class PoemResult(me.EmbeddedDocument):
    meta = {'allow_inheritance': True}

    content = me.ListField(me.ListField(me.StringField()))
    pronounce = me.ListField(me.StringField())
    score = me.IntField(default=0)


class VerseEightResult(PoemResult):
    perfect = me.ListField(me.IntField(default=0))
    inter_section_rhymes = me.ListField(me.IntField(default=0))
    intra_section_rhymes = me.ListField(me.IntField(default=0))
    inter_chapter_rhymes = me.ListField(me.IntField(default=0))
    alphabet_rhymes = me.ListField(me.IntField(default=0))
    tones = me.ListField(me.IntField(default=0))


class VerseFourResult(PoemResult):
    inter_section_rhymes = me.ListField(me.IntField(default=0))
    intra_section_rhymes = me.ListField(me.IntField(default=0))
    inter_chapter_rhymes = me.ListField(me.IntField(default=0))
    tone_rhymes = me.ListField(me.IntField(default=0))
    alphabet_rhymes = me.ListField(me.IntField(default=0))
    tones = me.ListField(me.IntField(default=0))


class Poem(me.Document, UserMixin):
    meta = {'collection': 'poems'}

    name = me.StringField()
    poem_type = me.StringField()
    is_private = me.BooleanField(default=False)
    author = me.ListField(me.ReferenceField('User', dbref=True))
    create_date = me.DateTimeField(required=True,
                                   default=datetime.now)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.now,
                                   auto_now=True)
    result = me.EmbeddedDocumentField(PoemResult)
    upvote = me.ListField(me.ReferenceField('User', dbref=True))
    comment = me.ListField(me.EmbeddedDocumentField(PoemComment))
