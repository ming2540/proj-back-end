from flask_mongoengine import MongoEngine
# from .oauth2 import OAuth2Token
from .users import User
from .poems import (Poem, VerseEightResult, VerseFourResult, PoemComment)
from .vocabs import Vocab

db = MongoEngine()

__all__ = [User, '''OAuth2Token''', Poem, VerseEightResult, VerseFourResult, Vocab]


def init_db(app):
    db.init_app(app)
