from flask import (Blueprint,
                   Flask,
                   redirect,
                   render_template,
                   url_for,
                   current_app,
                   json,
                   request,
                   g)
from flask_user import (login_required,
                        current_user)
from .. import models
import datetime
import poem_analyze
# from alglornrithm.forms import PoemForm
from ..ngram.word_suggestion import (word_suggestion, word_correction)
import dill as pickle
import random
from collections import defaultdict


module = Blueprint('poem', __name__, url_prefix='/poem')


@module.route('/', methods=['GET'])
def get_poems():
    offset = 5
    page = request.args.get('page')
    if not page:
        page = 0
    else:
        page = int(page)
    poems = models.Poem.objects().order_by('-update_date')[page*offset:page*offset+offset]
    poem_count = models.Poem.objects.count()
    is_next = True
    if page*offset + offset >= poem_count:
        is_next = False
    poem_response = {
        'result': poems,
        'count': poem_count,
        'next': is_next
    }
    return Flask.response_class(
        response=json.dumps(poem_response),
        status=200,
        mimetype='application/json'
    )


@module.route('/<poem_id>', methods=['GET'])
def get_poem(poem_id):
    poem = models.Poem.objects(id=poem_id).first()
    return Flask.response_class(
        response=poem.to_json(),
        status=200,
        mimetype='application/json'
    )


@module.route('/write_poem', methods=['GET', 'POST'])
def write_poem():
    if not request.method == 'POST':
        return Flask.response_class(
            response='method not allow',
            status=400,
            mimetype='application/json'
        )
    data = request.get_json()
    poem_type = 1
    if data['type'] == 'โคลงสี่สุภาพ':
        poem_type = 2
    poem_content = []
    content = []
    for syllabel in data['content']:
        string_content = []
        content.append(list(filter(None, syllabel)))
        for s in syllabel:
            string_content.append(s)
        poem_content.append(string_content)
    analyze_result = poem_analyze.poemAnalyze(poem_content, poem_type)
    if data['type'] == 'กลอนแปดสุภาพ':
        result = models.VerseEightResult(
            content=content,
            pronounce=analyze_result['pronounce'],
            score=analyze_result['score'],
            perfect=analyze_result['perfect'],
            inter_section_rhymes=analyze_result['inter_rhyme'],
            intra_section_rhymes=analyze_result['intra_rhyme'],
            inter_chapter_rhymes=analyze_result['inter_chapter_rhyme'],
            alphabet_rhymes=analyze_result['alphabet_rhyme'],
            tones=analyze_result['tone']
        )
    else:
        result = models.VerseFourResult(
            content=data['content'],
            pronounce=analyze_result['pronounce'],
            score=analyze_result['score'],
            inter_section_rhymes=analyze_result['inter_rhyme'],
            intra_section_rhymes=analyze_result['intra_rhyme'],
            inter_chapter_rhymes=analyze_result['inter_chapter_rhyme'],
            alphabet_rhymes=analyze_result['alphabet_rhyme'],
            tones=analyze_result['tone']
        )
    author = models.User.objects(id=data['author']).first()
    poem = models.Poem(name=data['name'],
                       poem_type=data['type'],
                       is_private=data['privacy'],
                       result=result)
    poem.author.append(author.id)

    # wait for user
    poem.save()
    author.poems.append(poem)
    author.save()

    # store_words(data['content'])
    print(word_correction(data['content']))

    # response_data = str(poem.id)
    response_data = {
        'id': str(poem.id),
        'suggest_word': word_correction(data['content'])
    }
    # print(response_data)

    return Flask.response_class(
            response=json.dumps(response_data),
            status=200,
            mimetype='application/json'
        )


@module.route('/append_poem/<poem_id>', methods=['POST'])
def append_poem(poem_id):
    if not request.method == 'POST':
        return Flask.response_class(
            response='method not allow',
            status=400,
            mimetype='application/json'
        )
    poem = models.Poem.objects(id=poem_id).first()
    if not poem:
        return Flask.response_class(
            response='Poem is not exist',
            status=404,
            mimetype='application/json'
        )
    if poem.is_private:
        return Flask.response_class(
            response='Poem is not allowed to append',
            status=400,
            mimetype='application/json'
        )
    data = request.get_json()
    poem.content = data['content']
    author = models.User.objects(id=data['author']).first()
    if author not in poem.author:
        poem.author.append(author)
    if poem.poem_type == 'กลอนแปดสุภาพ':
        result = poem_analyze.poemAnalyze(data['content'], 1)
    else:
        result = poem_analyze.poemAnalyze(data['content'], 2)
    poem.update(update_date=datetime.datetime.now())
    poem.update(set__result__content=data['content'],
                set__result__score=result['score'],
                set__result__perfect=result['perfect'],
                set__result__pronounce=result['pronounce'],
                set__result__inter_section_rhymes=result['inter_rhyme'],
                set__result__intra_section_rhymes=result['intra_rhyme'],
                set__result__inter_chapter_rhymes=result['inter_chapter_rhyme'],
                set__result__alphabet_rhymes=result['alphabet_rhyme'],
                set__result__tones=result['tone'])
    poem.save()
    response_data = {
        'id': str(poem.id),
        # 'suggest_word': word_correction(data['content'])
    }
    # print(response_data)

    return Flask.response_class(
            response=json.dumps(response_data),
            status=200,
            mimetype='application/json'
        )


@module.route('/suggest_word', methods=['GET', 'POST'])
def suggest_word():
    if not request.method == 'POST':
        return Flask.response_class(
            response='error',
            status=400,
            mimetype='application/json'
        )

    data = request.get_json()
    text = data
    result = word_suggestion(text)

    return Flask.response_class(
        response=json.dumps(result),
        status=200,
        content_type='application/json'
    )


@module.route('/comment/<poem_id>', methods=['POST'])
def comment(poem_id):
    data = request.get_json()
    poem = models.Poem.objects(id=poem_id).first()
    if not poem:
        return Flask.response_class(
            response='Poem is not exist',
            status=404,
            mimetype='application/json'
        )
    author = models.User.objects(id=data['author']).first()
    print(author)
    comment = models.PoemComment(
        author=author.id,
        content=data['content'],
    )
    poem.update(push__comment=comment)
    poem.save()
    author.comments.append(poem)
    author.save()
    response_data = {
        'id': str(poem.id),
        # 'suggest_word': word_correction(data['content'])
    }
    # print(response_data)

    return Flask.response_class(
            response=json.dumps(response_data),
            status=200,
            mimetype='application/json'
        )

@module.route('/upvote/<poem_id>', methods=['POST'])
def upvote(poem_id):
    data = request.get_json()
    poem = models.Poem.objects(id=poem_id).first()
    if not poem:
        return Flask.request_class(
            response='Poem is not exist',
            status=404,
            mimetype='application/json'
        )
    upvoter = models.User.objects(id=data['upvoter']).first()
    poem.upvote.append(upvoter)
    poem.save()
    response_data = {
        'id': str(poem.id)
    }
    return Flask.response_class(
        response=json.dumps(response_data),
        status=200,
        mimetype='application/json'
    )

@module.route('/unvote/<poem_id>', methods=['POST'])
def unvote(poem_id):
    data = request.get_json()
    poem = models.Poem.objects(id=poem_id).first()
    if not poem:
        return Flask.request_class(
            response='Poem is not exist',
            status=404,
            mimetype='application/json'
        )
    unvoter = models.User.objects(id=data['unvoter']).first()
    poem.update(pull__upvote=unvoter)
    poem.save()
    response_data = {
        'id': str(poem.id)
    }
    return Flask.response_class(
        response=json.dumps(response_data),
        status=200,
        mimetype='application/json'
    )


# def get_ngram():
#     if 'ngram' not in g:
#         g.ngram = defaultdict(lambda: defaultdict(lambda: 0))
#         g.ngram = pickle.load(open('alglornrithm/ngram/model.pkl', 'rb'))
#     print('imported ngram to g')
#     return g.ngram

